---
title: "Slider 1"
draft: false
bg: "from-[#120169] to-[#0BDBCA]"
btn_text_1: "BOTON 1.1"
btn_text_2: "BOTON 1.2"
btn_bg_1: "bg-[#0BBDE6]"
btn_bg_2: "bg-white"
href_1: "#"
href_2: "#"
img: "images/rocket-icon.png"
---

SLIDER 1