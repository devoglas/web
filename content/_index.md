---
title: "Devoglas"
draft: false
primeros:
- titulo: titulo 1
  texto: texto 1
  img: images/pc-icon.png
- titulo: titulo 2
  texto: texto 2
  img: images/pc-icon.png
- titulo: titulo 3
  texto: texto 3
  img: images/pc-icon.png
- titulo: titulo 4
  texto: texto 4
  img: ""

segundos:
- titulo: titulo
  texto: texto
  detalle: detalle
- titulo: titulo
  texto: texto
  detalle: detalle
- titulo: titulo
  texto: texto
  detalle: detalle
- titulo: titulo
  texto: texto
  detalle: detalle

terceros:
- titulo: titulo 3
  texto: texto 3 
- titulo: titulo 3
  texto: texto 3 
- titulo: titulo 3
  texto: texto 3 

---