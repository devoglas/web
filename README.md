# Web Devoglas

This project use:
- Docker
- docker-compose
- HUGO Framework
- Tailwind Framework

You need to have installed docker, docker-compose and npm to start working with this project.
## Installation/Use

To begin with, in the terminal go to the project folder to install the dependencies, type:
```
npm install
```

Then, to start the environment type on the terminal:

```
$ docker-compose up -d
```

This will download and execute the docker container(s), and execute on the background until is stopped by the user or the PC is shutdown.

Once the container is running, type the following URL on the web browser: `localhost:1313`, there you can visualize the site.

To style with tailwind, you need to start tailwindcss in watch mode, type this in the terminal:
```
npx tailwindcss -i ./static/styles/style.css -o ./static/styles/tailwind.css --watch
```

This execute tailwind compiler, that will compile any utility applied to the html files to the output file tailwind.css.

Pipeline available
