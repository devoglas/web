module.exports = {
  important: true,
  content: [
    "layouts/partials/**/*.html",
    "layouts/*.html",
    "layouts/shortcodes/*.html",
    "content/*.md"
  ],
  mode: "jit",
  theme: {
    extend: {
      fontFamily: {
        'raleway': 'Raleway',
      },
      colors: {
        devoglas: {
          main: '#000048',
          second: '#0B4AE6',
          third: '#343A40'
        }
      }
    },
  },
  plugins: [],
}